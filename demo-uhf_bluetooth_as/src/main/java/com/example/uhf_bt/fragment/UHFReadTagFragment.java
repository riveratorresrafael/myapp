package com.example.uhf_bt.fragment;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.provider.OpenableColumns;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;


import com.example.uhf_bt.DateUtils;
import com.example.uhf_bt.FileUtils;
import com.example.uhf_bt.MainActivity;
import com.example.uhf_bt.R;
import com.example.uhf_bt.Utils;
import com.google.gson.Gson;
import com.rscja.deviceapi.RFIDWithUHFBluetooth;
import com.rscja.deviceapi.entity.UHFTAGInfo;

import java.io.File;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.TimerTask;

import androidx.fragment.app.Fragment;

import javax.annotation.Nullable;


public class UHFReadTagFragment extends Fragment implements View.OnClickListener{

    private boolean loopFlag = false;
    private Button InventoryLoop, btStop;//
    private boolean isExit = false;
    private MainActivity mContext;
    private HashMap<String, String> map;
    private ArrayList<HashMap<String, String>> tagList;
    private String TAG = "DeviceAPI_UHFReadTag";

    private DateTimeFormatter formatter;

    //--------------------------------------获取 解析数据-------------------------------------------------
    final int FLAG_START = 0;//开始
    final int FLAG_STOP = 1;//停止
    final int FLAG_UHFINFO = 3;
    final int FLAG_SUCCESS = 10;//成功
    final int FLAG_FAIL = 11;//失败

    boolean isRuning = false;
    //--------------------------------
    private static int VIDEO_REQUEST=101;

    Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case FLAG_STOP:
                    if (msg.arg1 == FLAG_SUCCESS) {
                        //停止成功
                        btStop.setEnabled(false);
                        InventoryLoop.setEnabled(true);
                    } else {
                        //停止失败
                        Utils.playSound(2);
                        Toast.makeText(mContext, R.string.uhf_msg_inventory_stop_fail, Toast.LENGTH_SHORT).show();
                    }
                    break;
                case FLAG_START:
                    if (msg.arg1 == FLAG_SUCCESS) {
                        //开始读取标签成功
                        btStop.setEnabled(true);
                        InventoryLoop.setEnabled(false);
                    } else {
                        //开始读取标签失败
                        Utils.playSound(2);
                    }
                    break;
                case FLAG_UHFINFO:
                    UHFTAGInfo info = (UHFTAGInfo) msg.obj;
                    addEPCToList(info);
                    Utils.playSound(1);
                    break;
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_uhfread_tag, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.i(TAG, "UHFReadTagFragment.onActivityCreated");
        super.onActivityCreated(savedInstanceState);
        mContext = (MainActivity) getActivity();
        init();
        formatter = DateTimeFormatter.ofPattern("HH:mm:ss:SSS");
    }

    @Override
    public void onPause() {
        super.onPause();
        //stopInventory();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        isExit = true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.InventoryLoop:
                configReader();
                startThread(false);
                Intent videoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                if (videoIntent.resolveActivity(mContext.getPackageManager())!=null){
                    startActivityForResult(videoIntent,VIDEO_REQUEST);
                }
                break;
            case R.id.btStop:
                if (mContext.uhf.getConnectStatus() == RFIDWithUHFBluetooth.StatusEnum.CONNECTED) {
                    startThread(true);
                }
                clearData();
                mContext.showToast("Operación cancelada-JSON no guardado");
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode==VIDEO_REQUEST){
            if (mContext.uhf.getConnectStatus() == RFIDWithUHFBluetooth.StatusEnum.CONNECTED) {
                startThread(true);
            }
            if(resultCode== Activity.RESULT_OK){
                Cursor cursor = mContext.getContentResolver().query(data.getData(), null, null, null, null);
                int nameIndex = cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME);
                cursor.moveToFirst();
                saveJSON(cursor.getString(nameIndex).split("\\.")[0]);
            }else if(resultCode==Activity.RESULT_CANCELED){
                clearData();
                mContext.showToast("Operación cancelada-JSON no guardado");
            }
        }
    }
    private void configReader(){
        //set power
        int iPower = 30;
        if (!mContext.uhf.setPower(iPower)) {
            Toast.makeText(mContext, R.string.uhf_msg_set_power_fail, Toast.LENGTH_SHORT).show();
        }
        //set frec
        byte f = 0x02;
        if (!mContext.uhf.setFrequencyMode(f)) {
            mContext.showToast(R.string.uhf_msg_set_frequency_fail);
        }
        //set protocol
        int prot = 0;
        if (!mContext.uhf.setProtocol(prot)) {
            //mContext.showToast(R.string.uhf_msg_get_protocol_fail);
        }
    }
    private void saveJSON(String vid_name){
        try {
            //Toast.makeText(mContext,"primer elemento:"+tagList.get(0).get("tagUii")+"..."+tagList.get(0).get("lat"),Toast.LENGTH_SHORT).show();
            String name= vid_name + "-" + String.valueOf(mContext.sp_sector.getSelectedItemPosition()+1)+".json";
            Gson gson = new Gson();
            String json= gson.toJson(tagList);
            String route = Environment.getExternalStorageDirectory() + File.separator + name;
            FileUtils.writeFile(route, json, true);
            /*
            for (int i = 0; i < tagList.size(); i++) {
                myRef.push().setValue(tagList.get(i));
            }
            */
            mContext.showToast("JSON creado");
            clearData();
        }catch (Exception e){
            mContext.showToast("ERROR...JSON no guardado");
        }
    }


    private void init() {
        isExit = false;
        setConnectStatusNotice();
        InventoryLoop = (Button) mContext.findViewById(R.id.InventoryLoop);
        btStop = (Button) mContext.findViewById(R.id.btStop);
        btStop.setEnabled(false);

        InventoryLoop.setOnClickListener(this);
        btStop.setOnClickListener(this);

        tagList = new ArrayList<>();
        mContext.uhf.setKeyEventCallback(new RFIDWithUHFBluetooth.KeyEventCallback() {
            @Override
            public void getKeyEvent(int keycode) {
                Log.d("DeviceAPI_ReadTAG", "  keycode =" + keycode + "   ,isExit=" + isExit);
                if (!isExit && mContext.uhf.getConnectStatus() == RFIDWithUHFBluetooth.StatusEnum.CONNECTED) {
                    startThread(loopFlag);
                }
            }
        });
        clearData();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mContext.uhf.getConnectStatus() == RFIDWithUHFBluetooth.StatusEnum.CONNECTED) {
            InventoryLoop.setEnabled(true);
        } else {
            InventoryLoop.setEnabled(false);
        }
        setConnectStatusNotice();
    }

    private void setConnectStatusNotice() {
        if(mContext != null)
            mContext.setConnectStatusNotice(new ConnectStatus());
    }

    private void clearData() {
        tagList.clear();
    }

    /**
     * 停止识别
     */
    private void stopInventory() {
        cancelInventoryTask();
        loopFlag = false;
        if(mContext.scaning) {
            RFIDWithUHFBluetooth.StatusEnum statusEnum = mContext.uhf.getConnectStatus();
            Message msg = handler.obtainMessage(FLAG_STOP);
            boolean result = mContext.uhf.stopInventoryTag();
            if (result || statusEnum == RFIDWithUHFBluetooth.StatusEnum.DISCONNECTED) {
                msg.arg1 = FLAG_SUCCESS;
            } else {
                msg.arg1 = FLAG_FAIL;
            }
            if (statusEnum == RFIDWithUHFBluetooth.StatusEnum.CONNECTED) {
                //在连接的情况下，结束之后继续接收未接收完的数据
            }
            mContext.scaning = false;
            handler.sendMessage(msg);
        }
    }

    class ConnectStatus implements MainActivity.IConnectStatus {
        @Override
        public void getStatus(RFIDWithUHFBluetooth.StatusEnum statusEnum) {
            if (statusEnum == RFIDWithUHFBluetooth.StatusEnum.CONNECTED) {
                if (!loopFlag) {
                    try {
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    InventoryLoop.setEnabled(true);
                }
            } else if (statusEnum == RFIDWithUHFBluetooth.StatusEnum.DISCONNECTED) {
                loopFlag = false;
                mContext.scaning = false;
                btStop.setEnabled(false);
                InventoryLoop.setEnabled(false);
            }
        }
    }


    public void startThread(boolean isStop) {
        if (isRuning) {
            return;
        }
        isRuning = true;
        new TagThread(isStop).start();
    }

    class TagThread extends Thread {
        boolean isStop = false;

        public TagThread(boolean isStop) {
            this.isStop = isStop;
        }

        public void run() {
            if (isStop) {
                stopInventory();
                isRuning = false;//执行完成设置成false
            } else {
                Message msg = handler.obtainMessage(FLAG_START);
                if (mContext.uhf.startInventoryTag()) {
                    loopFlag = true;
                    mContext.scaning = true;
                    //mStrTime = System.currentTimeMillis();
                    msg.arg1 = FLAG_SUCCESS;
                } else {
                    msg.arg1 = FLAG_FAIL;
                }
                handler.sendMessage(msg);
                isRuning = false;//执行完成设置成false
                while (loopFlag) {
                    getUHFInfo();
                }
            }
        }
    }

    private boolean getUHFInfo() {
        ArrayList<UHFTAGInfo> list = mContext.uhf.readTagFromBuffer();
        if (list != null) {
            for (int k = 0; k < list.size(); k++) {
                UHFTAGInfo info = list.get(k);
                Message msg = handler.obtainMessage(FLAG_UHFINFO);
                msg.obj = info;
                handler.sendMessage(msg);
                if(!loopFlag) {
                    break;
                }
            }
            if (list.size() > 0)
                return true;
        } else {
            return false;
        }
        return false;
    }

    /**
     * 添加EPC到列表中
     *
     * @param uhftagInfo
     */
    private void addEPCToList(UHFTAGInfo uhftagInfo) {
        if (!TextUtils.isEmpty(uhftagInfo.getEPC())) {
            int index = checkIsExist(uhftagInfo.getEPC());

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(uhftagInfo.getEPC());

            if (!TextUtils.isEmpty(uhftagInfo.getTid())) {
                stringBuilder.append("-");
                stringBuilder.append(uhftagInfo.getTid());
            }
            if (!TextUtils.isEmpty(uhftagInfo.getUser())) {
                stringBuilder.append("-");
                stringBuilder.append(uhftagInfo.getUser());
            }

            map = new HashMap<String, String>();
            map.put("tagUii", uhftagInfo.getEPC());
            map.put("tagData", stringBuilder.toString());
            if (index == -1) {
                map.put("dateTime",formatter.format(LocalDateTime.now()));
                tagList.add(map);
                //Toast.makeText(mContext,"tag agregado contiene:"+map.get("tagUii")+"..."+map.get("lat"),Toast.LENGTH_LONG).show();
            } else {
                map.put("dateTime",formatter.format(LocalDateTime.now()));
                tagList.set(index, map);
            }
        }
    }

    public int checkIsExist(String strEPC) {
        int existFlag = -1;
        if (strEPC == null || strEPC.isEmpty()) {
            return existFlag;
        }
        String tempStr = "";
        for (int i = 0; i < tagList.size(); i++) {
            tempStr = tagList.get(i).get("tagUii");
            if (strEPC.equals(tempStr)) {
                existFlag = i;
                break;
            }
        }
        return existFlag;
    }

    private TimerTask mInventoryPerMinuteTask;
    private void cancelInventoryTask() {
        if(mInventoryPerMinuteTask != null) {
            mInventoryPerMinuteTask.cancel();
            mInventoryPerMinuteTask = null;
        }
    }

}
